
#include <Arduino.h>

#ifndef __SENDER_H__
#define __SENDER_H__


class Receiver
{
	//variables
	public:
	protected:
	private:
	short int yaw;
	short int roll;
	short int pitch;
	short int altitude;
	short int timeout;
	const short int maxTimeout = 1000;
	const float deg_2_rad = 1000.0 / 57296.0;

	//functions
	public:
	Receiver();
	~Receiver();
	void CommandRemote(String command, String awnser, String info1, String info2);
	void Get_Request_Ypra(float *ypra, unsigned short dt);
	void Print_SerialOscilloscope();
    unsigned char checkSum( char *x, int size );
    void GetMessage(unsigned short dt);
	protected:
	private:
	
	void GetErrorMsg( unsigned char msg);
	Receiver( const Receiver &c );
	Receiver& operator=( const Receiver &c );

}; //Sender

#endif //__SENDER_H__

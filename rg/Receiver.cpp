#include "Receiver.h"
#include <Arduino.h>

// default constructor
Receiver::Receiver()
{
} //Sender

// default destructor
Receiver::~Receiver()
{
} //~Sender

void Receiver::CommandRemote(String command, String awnser, String info1, String info2) {
	Serial.println(info1);
	bool commandDone = false;
	while (!commandDone ) {
		Serial.println("{" + command + "}");
		if (Serial.available()) {
		String data = (Serial.readStringUntil('}'));
		data = data.substring(data.indexOf("{") + 1);
			if (data == awnser) {
				commandDone = true;
			}
		}
		delay(300);
	}//close while
	Serial.println(info2);
}

unsigned char Receiver::checkSum( char *x, int size )
{
  unsigned int sum=0;
  char curr_value;
  for( int i = 0; i< size; i++){
    curr_value = x[i];
    while( curr_value > 0x0 ){
      sum +=  (curr_value & 0x01);
      curr_value >>=  1;
    }
  }

  return sum;
}

void Receiver::GetErrorMsg( unsigned char msg){
	switch(msg){
		case 0x02:
			Serial.print("Law battery \t");
			break;
		case 0x04:
			Serial.print("Emergency stop \t");
			break;
		case 0x06:
			Serial.print("Law battery & emergency \t");
			break;
		case 0x08:
			Serial.print("Genereal error msg \t");
			break;
		case 0x0e:
			Serial.print("Law battery  & emergency & errorgeneral error\t");
			break;
	}
}


void Receiver::GetMessage(unsigned short dt) {
	
	//
    if(Serial2.available( )> 12){
        short int pitch_y, roll_x;
        char data[12];
        Serial2.readBytes(data, 12);
        int check_sum = checkSum(data, 9);		
        //if( 0x01 == data[0] && check_sum == data[9]){ 			           
		if(true){
            yaw = (0x0000 | (data[1] << 8) | data[2]);
            pitch_y = (0x0000 | data[3] << 8) | data[4];
            roll_x = (0x0000 | data[5] << 8) | data[6];    
            altitude = (0x0000 | data[7] << 8) | data[8];   
			// Add decoding of error msgs
			unsigned char err_msg  = data[10] && 0x0e;
			if( err_msg != 0){
				GetErrorMsg(err_msg);
			}    
			unsigned char com_msg = data[10] && 0xf0;
			com_msg = com_msg >> 4;
			if( com_msg != 0){
				//GetCommand();
			}
		} 
		for(int i =0 ; i< 11; i++){
			Serial.print(data[i], HEX);
		}
		Serial.println("\n");
        //transformation of ypr around z axis;
        float angle = 0.0 * deg_2_rad;
        roll = roll_x * cos(angle) + pitch_y * cos(angle);
        pitch = -roll_x  * sin(angle) + pitch_y * cos(angle);

        // timeout
        timeout = 0; 
    }else
		timeout += dt;
	
}

void Receiver::Get_Request_Ypra(float *ypra, unsigned short dt){
	
	GetMessage(dt);
	if(timeout > maxTimeout){
		ypra[0] = 0;
		ypra[1] = 0;
		ypra[2] = 0;
		if(altitude>0)
		    altitude = altitude -1;
		else
    		altitude=0;

		ypra[3] = altitude;
		
	}
	else
	{
		ypra[0] = yaw;
		ypra[1] = pitch;
		ypra[2] = roll;
		ypra[3] = altitude;
	}	
}

void Receiver::Print_SerialOscilloscope(){

	Serial.print("ypra Requests\t");
	Serial.print(yaw);
	Serial.print(",");
	Serial.print(pitch);
	Serial.print(",");
	Serial.print(roll);
	Serial.print(",");
	Serial.print(altitude);
	Serial.println(",");
}


